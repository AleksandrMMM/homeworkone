package homeworksone;

import java.util.Arrays;

public class HomeworkOne {

    public static void main(String[] args) {

        System.out.println(ex1());

        ex2();

        ex3();
    }

    public static String ex1() {
        //Дана строка
        String name = "     ПЕтРов Олег Иванович     ";

        String nameTwo = name.trim().toUpperCase();

        String[] s = nameTwo.split(" ");

        if (s[0].endsWith("ОВ")) {

            return "Уважаемый " + nameTwo;
        } else if (s[0].endsWith("ОВА")) {

            return "Уважаемая " + nameTwo;
        }

        return "Неизвестное лицо " + nameTwo;
        //Необходимо
        //1. убрать лишние пробелы,
        //2. перевести текст в верхний регистр
        //3. Если содержит "ова " то печатаем на экран: Уважаемая {name}
        // Если содержит "ов " то печатаем на экран: Уважаемый {name}
        // В иных случаях печатаем на экран: Неизвестное лицо {name}
    }

    public static void ex2() {
        //У нас есть машина. В данной машине есть есть перечень проверок, перед запуском
        //Количество топлива
        double fuel = 10;
        //Проверка двигателя
        boolean isEngineWork = true;
        //Проверка отсутствия ошибок (false - ошибок нет)
        boolean hasErrors = false;
        //Датчики давления шин
        boolean isWheelWork1 = true;
        boolean isWheelWork2 = true;
        boolean isWheelWork3 = true;
        boolean isWheelWork4 = true;

        //Поменять(убрать, поставить) логические операторы так, чтобы машина запускалась:
        // когда топлива не меньше 10 литров, двигатель работает, колеса все работают, нет ошибок
        //В ином случае, машина не должна запускаться
        if (
                fuel > 10
                        ||  (!isWheelWork1 || isWheelWork2 || isWheelWork3 || isWheelWork4)
                        && hasErrors
                        || isEngineWork
        ) {
            System.out.println("Машина работает");
        } else {
            System.out.println("Машина не работает");
        }
    }

    public static void ex3() {
        //Работа на самостоятельное изучение методов.
        //Заменить в строке все 'this is' на 'those are', получить индекс (число) второй буквы 'o' в строке
        //Распечатать полученный индекс
        String simply = "this is simply. This is my favorite song.";

        simply.replaceAll("this is", "those are");
        int i = simply.indexOf('o', simply.indexOf('o') + 1);
        System.out.println(i);

    }

}
