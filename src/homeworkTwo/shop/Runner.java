package homeworkTwo.shop;

import java.util.List;

public class Runner {

    public static void main(String[] args) {


        var itemOne = new Item("молот", 1000);
        var itemTwo = new Item("отвертка", 50);
        var itemFree = new Item("тетрадь", 30);
        var itemFour = new Item("ручка", 10);

        var worker = new Worker("Василий", 30, "мужчина", List.of(itemOne, itemTwo));
        var worker1 = new Worker("Марьяна", 25, "женщина", List.of(itemFree, itemFour));

        var shop = new Shop(List.of(worker, worker1));

        shop.printWorkers();
        shop.getWorkers().forEach(Worker::getOrr);
    }
}
