package homeworkTwo.shop;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@AllArgsConstructor
public class Shop {

     private List<Worker> workers = new ArrayList<>();

     public void printWorkers() {

         workers.forEach(w -> System.out.println(w.getName() + ":" + w.getAge() + " лет," +  w.getSex() + ", список вещей: " + w.getItems()));
     }
}
