package homeworkTwo.shop;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Worker {


    private String name;
    private int age;
    private String sex;
    private List<Item>  items = new ArrayList<>();

    public void getOrr() {

        items.forEach(v -> System.out.println(this.name + " : смотрите какая у меня вещь - " + v));
    }

    @Override
    public String toString() {
        return "Worker{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", items=" + items +
                '}';
    }
}
