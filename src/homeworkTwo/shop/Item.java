package homeworkTwo.shop;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Item {

    private String name;
    private int weight;

    @Override
    public String toString() {
        return  name;
    }
}
